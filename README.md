# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Binary search tree(BST) is a efficient method of storing values in tree like data structure. It allows fast search, addition and removal of values is easy. It is consist of nodes, with  each node indicating by a key. Searching a value is a recursive and iterative call. Or you can call it a pre-order traversal which means that you have to visit left side of the tree(first), the root node(second) and right side of the root node (third). Know more about this code and other searching algorithms at [Chicago Excel classes](http://www.chicagocomputerclasses.com/excel-classes/).

![clip_image004_thumb[1].gif](https://bitbucket.org/repo/ML5bL8/images/1904474628-clip_image004_thumb%5B1%5D.gif)

![252_b.gif](https://bitbucket.org/repo/ML5bL8/images/328819754-252_b.gif)

![bst-search.png](https://bitbucket.org/repo/ML5bL8/images/2106093879-bst-search.png)
