/**
 * This class is the shell for the Binary Search Tree. It has
 * methods to change the root, insert values, delete leaves,
 * determine if the tree is empty, and traverse the tree in
 * the pre/in/post order traversals.
 */

import java.util.ArrayList;

public class Tree {
	
	private TreeNode root; //holds the root node
	private ArrayList<Integer> values; //holds all the values in the list
	
	//constructor... a new tree makes a null root
	public Tree(){
		root = null;
	}

	//allows user to set root
	public void setRoot(TreeNode r){
		root = r;
	}
	
	//allows user to get root
	public TreeNode getRoot(){
		return root;
	}
	
	//sets the values list arraylist to an empty list.
	//this is used as a check before insertion and deletion
	public void setValuesList(){
		values = new ArrayList<Integer>();
	}
	
	//returns true if the tree is empty
	public boolean isEmpty(){
		return (root == null);
	}
	
	//insertion method. 
	public void insert(int value){
		//if determines if value is already in tree, breaks method if it is
		if (values.indexOf(value) != -1){
			System.out.println();
			System.out.println(value+" is already in the tree.");
			return;
		}
		//if tree is empty, set value as root, else add with helper method
		if (root == null){
			root = new TreeNode(value);
			values.add(value);
		} else {
			insertHelper(root, value);
			values.add(value);
		}
		System.out.println();
		System.out.println(value+" has been added succesfully!");
	}
	
	//recursive BST insertion method
	public void insertHelper(TreeNode node, int value){
		if (value < node.getValue()){
			if (node.getLeft() == null){
				node.setLeft(new TreeNode(value));
			} else {
				insertHelper(node.getLeft(), value);
			}
		} else {
			if (node.getRight() == null){
				node.setRight(new TreeNode(value));
			} else {
				insertHelper(node.getRight(), value);
			}
		}
	}
	
	//removes leaves from tree
	public void deleteNode(int value){
		//if checks if value is not in tree, breaks if it isn't
		if (values.indexOf(value) == -1){
			System.out.println();
			System.out.println(value+" is not in the tree.");
			return;
		}
		//set tempNode to root and step through tree until leaf is found.
		//then set the node before the leaf's left/right to null.
		TreeNode tempNode = root;
		while(true){
			if (value < tempNode.getValue() && tempNode.getLeft().getValue() != value){
				tempNode = tempNode.getLeft();
			} else if (value > tempNode.getValue() && tempNode.getRight().getValue() != value){
				tempNode = tempNode.getRight();
			} else if (tempNode.getLeft() != null && tempNode.getLeft().getValue() == value && tempNode.getLeft().getLeft() == null && tempNode.getLeft().getRight() == null){
				tempNode.setLeft(null);
				values.remove(values.indexOf(value));
				System.out.println();
				System.out.println(value+" has succesfully been deleted!");
				return;
			} else if (tempNode.getRight() != null && tempNode.getRight().getValue() == value && tempNode.getRight().getLeft() == null && tempNode.getRight().getRight() == null){
				tempNode.setRight(null);
				values.remove(values.indexOf(value));
				System.out.println();
				System.out.println(value+" has succesfully been deleted!");
				return;
			}
		}
	}
	
	//recursive inOrderTraversal
	public void inOrderTraversal(TreeNode node){
		if (isEmpty()){
			System.out.println("Tree is empty.");
			return;
		}
		if (node != null){
			inOrderTraversal(node.getLeft());
			System.out.println(node.getValue());
			inOrderTraversal(node.getRight());
		}
	}
	
	//recursive preOrderTraversal
	public void preOrderTraversal(TreeNode node){
		if (isEmpty()){
			System.out.println("Tree is empty.");
			return;
		}
		if (node != null){
			System.out.println(node.getValue()); 
			preOrderTraversal(node.getLeft());
			preOrderTraversal(node.getRight());
		}
	}
	
	//recursive postOrderTraversal
	public void postOrderTraversal(TreeNode node){
		if (isEmpty()){
			System.out.println("Tree is empty.");
			return;
		}
		if (node != null){
			postOrderTraversal(node.getLeft());
			postOrderTraversal(node.getRight());
			System.out.println(node.getValue());
		}
	}
	
}
